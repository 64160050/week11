package com.tanisorn.week11;

public class Crocodile extends Animal implements Swimable, Crawlable {

    public Crocodile(String name) {
        super(name, 4);
        
    }


    @Override
    public void Crawl() {
        System.out.println(this + " Crawl.");
        
    }

    @Override
    public void swim() {
        System.out.println(this + " swim.");
        
    }

    @Override
    public void eat() {
        System.out.println(this + " eat.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this + " sleep.");
        
    }
}
