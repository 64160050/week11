package com.tanisorn.week11;


public class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();
        bat1.fly();
        bat1.landing();
        bat1.takeoff();

        Fish fish1 = new Fish("Nemo");
        fish1.eat();
        fish1.sleep();
        fish1.swim();

        Plane plane1 = new Plane("Nok Air", "Nok Air engine");
        plane1.fly();
        plane1.landing();
        plane1.takeoff();

        Submarine submarine1 = new Submarine("Paryud", "Paryudengine");
        submarine1.swim();

        Crocodile crocodile1 = new Crocodile("Benz");
        crocodile1.Crawl();
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.swim();

        Snake snake1 = new Snake("Khiao");
        snake1.Crawl();
        snake1.eat();
        snake1.sleep();

        Bird bird1 = new Bird("Parrot");
        bird1.eat();
        bird1.sleep();
        bird1.fly();
        bird1.landing();
        bird1.takeoff();

        Rat rat1 = new Rat("Jerry");
        rat1.walk();
        rat1.run();
        rat1.eat();
        rat1.sleep();

        Human human1 = new Human("Tong");
        human1.walk();
        human1.run();
        human1.eat();
        human1.sleep();

        Dog dog1 = new Dog("Dang");
        dog1.walk();
        dog1.run();
        dog1.eat();
        dog1.sleep();

        Cat cat1 = new Cat("Tom");
        cat1.walk();
        cat1.run();
        cat1.eat();
        cat1.sleep();
        
        Flyable[] flyableObjects = {bat1, plane1, bird1};
        for(int i = 0; i < flyableObjects.length; i++) {
            flyableObjects[i].fly();
            flyableObjects[i].landing();
            flyableObjects[i].takeoff();
        }


        Swimable[] swimableObjects = {fish1 , submarine1 , crocodile1};
        for(int i = 0; i < swimableObjects.length; i++) {
            swimableObjects[i].swim();
        }


        Crawlable[] crawlableObjects = {crocodile1, snake1};
        for(int i = 0; i < crawlableObjects.length; i++) {
            crawlableObjects[i].Crawl();
        }

        Walkable[] walkableObjects = {rat1, human1, dog1, cat1};
        for(int i = 0; i < walkableObjects.length; i++) {
            walkableObjects[i].walk();
            walkableObjects[i].run();
        }
    }
}
