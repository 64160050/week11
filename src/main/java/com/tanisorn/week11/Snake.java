package com.tanisorn.week11;

public class Snake extends Animal implements Crawlable{

    public Snake(String name) {
        super(name, 0);
    }

    @Override
    public void Crawl() {
        System.out.println(this + " Crawl.");
        
    }

    @Override
    public void eat() {
        System.out.println(this + " eat.");
        
    }

    @Override
    public void sleep() {
        System.out.println(this + " sleep.");
        
    }
    
}
